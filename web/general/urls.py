from django.conf.urls import url
from . import views


urlpatterns = [
	url(r'^$', views.index),
	url(r'^temperatura$', views.get_temp),
	url(r'^dotyk$', views.get_touch)
]