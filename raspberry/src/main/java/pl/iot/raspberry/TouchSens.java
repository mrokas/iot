package pl.iot.raspberry;
import com.pi4j.io.gpio.GpioController;
import com.pi4j.io.gpio.GpioFactory;
import com.pi4j.io.gpio.GpioPinDigitalInput;
import com.pi4j.io.gpio.GpioPinDigitalOutput;
import com.pi4j.io.gpio.PinPullResistance;
import com.pi4j.io.gpio.PinState;
import com.pi4j.io.gpio.RaspiPin;
import com.pi4j.io.gpio.event.GpioPinDigitalStateChangeEvent;
import com.pi4j.io.gpio.event.GpioPinListenerDigital;
import static java.lang.Boolean.TRUE;
import static java.lang.Thread.sleep;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;


public class TouchSens extends Thread {
    public void run(){
        final GpioController gpio = GpioFactory.getInstance();
        final GpioPinDigitalInput touchSens1 = gpio.provisionDigitalInputPin(RaspiPin.GPIO_23,             // PIN NUMBER
                                                                     "TouchSensor",                   // PIN FRIENDLY NAME (optional)
                                                                     PinPullResistance.PULL_UP); // PIN RESISTANCE (optional)
        touchSens1.setShutdownOptions(true);
        touchSens1.setDebounce(3000,PinState.HIGH);
        touchSens1.setDebounce(3000,PinState.LOW);
         
		ResultSet rs = null;
      
		try {
			final Connection conn = DriverManager.getConnection("jdbc:mysql://mrokas.ayz.pl/mrokas_iot?user=mrokas_iot&password=testowe12");
			final Statement stmt = conn.createStatement();
			
            touchSens1.addListener(new GpioPinListenerDigital() {
				
				public void handleGpioPinDigitalStateChangeEvent(GpioPinDigitalStateChangeEvent event) {
					
					DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
					Date date = new Date();
					String dataa = dateFormat.format(date);
					String temp = "0";
					
					if(event.getState() == PinState.LOW){
						System.out.println("asjdfgkahgjka");
					} 
					
					
					if(event.getState() ==PinState.HIGH){
						temp = "1";
						
						try {
							
							stmt.executeUpdate("INSERT INTO touched (touch, date) VALUES ('" + temp + "', '" + dataa + "');");
							System.out.println(" --> GPIO PIN STATE CHANGE: " + event.getPin() + " = " + event.getState());
							Thread.sleep(3000);
							
						} catch (SQLException | InterruptedException ex) {
							Logger.getLogger(TouchSens.class.getName()).log(Level.SEVERE, null, ex);
						}
					}
				}
			});

        } catch (SQLException ex) {
            Logger.getLogger(TouchSens.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    
    }

