package pl.iot.raspberry;

import com.github.theholywaffle.teamspeak3.*;
import com.github.theholywaffle.teamspeak3.api.TextMessageTargetMode;
import com.github.theholywaffle.teamspeak3.api.event.TS3EventAdapter;
import com.github.theholywaffle.teamspeak3.api.event.TS3EventType;
import com.github.theholywaffle.teamspeak3.api.event.TextMessageEvent;
import java.io.*;


public class Main {
    public static String battlefyLink = "Jeszcze nie podano";

    public static String processText(String message, String invokingUser, TS3Api api){
        String temp = "";
        String[] helloArray = {"hello", "hi", "hej", "hey", "yo", "elo", "siema", "cze"};


        if (message.equals("!ping")) {
            return "pong!";
        }

        if (message.equals("!temp")) {
            float temperature = 0;
            String strLine = "";
            try{
                FileInputStream fstream = new FileInputStream("/sys/bus/w1/devices/28-0516934116ff/w1_slave");
                BufferedReader br = new BufferedReader(new InputStreamReader(fstream));
                int i = 0;
                while ((strLine = br.readLine()) != null)   {
                    if(i == 1){
                        String[] parts = strLine.split("=");
                        temperature = Float.parseFloat(parts[1]);
                        temperature = temperature/1000;
                    }
                    i++;
                }

                br.close();
            }catch(Exception e){}

            return "Temperatura w pokoju: "+temperature+ " stopni Celsjusza";
        }

        if (message.contains("battlefy.com")) {
            // Answer "!ping" with "pong"
            battlefyLink = message.toString();
            return "";
        }

        if (message.contains("nsfw")) {
            // Answer "!ping" with "pong"
            Boolean a;
            api.kickClientFromChannel("tak nie wolno!",api.getClientByNameExact(invokingUser, true));
            return "";
        }

        if (message.equals("!battlefy")) {
            // Answer "!ping" with "pong"
            return battlefyLink;
        }        


        for(int k = 0; k < helloArray.length; k++)
            if(message.startsWith(helloArray[k]))
                return "Hello "+invokingUser+"!";

        return "";
    }
    
    public static void main(String[] args) {
        //String channelName = "Senzu Bean Cartel [NIE PRZEKLINAMY]"; //285
        // String channelName = "Sala Tronowa [NIE PRZEKLINAMY]"; //283

        CyclicSaver cSaver = new CyclicSaver();
        cSaver.start();
        
        TouchSens tsensor = new TouchSens();
        tsensor.start();
        
        final TS3Config config = new TS3Config();
        config.setHost("yds.com.pl");
        config.setQueryPort(10011);
        //config.setEnableCommunicationsLogging(true);

        final TS3Query query = new TS3Query(config);
        query.connect();

        final TS3Api api = query.getApi();
        api.login("Revvy", "697NsXF1");
        api.selectVirtualServerById(1);
        api.moveQuery(283);
        api.setNickname("TSowy Guru");
        //api.sendChannelMessage("Guru wkracza do akcji!");
        //api.sendChannelMessage("https://battlefy.com/turniejeonlineeu/turniejeonlineeu-5vs5-166-eune/59d8e4d2cd84b30349bd9ab9/info?infoTab=details");



        /**  List<Channel> channels = api.getChannels();

        for (Channel a : channels){
            if (a.getName().equalsIgnoreCase(channelName)){
                System.out.println(a.getId());
            }
        }
        System.out.println(channels.get(0).getName());**/

        // Get our own client ID by running the "whoami" command
        final int clientId = api.whoAmI().getId();

        // Listen to chat in the channel the query is currently in
        api.registerEvent(TS3EventType.TEXT_CHANNEL, 0);

        // Register the event listener
        api.addTS3Listeners(new TS3EventAdapter() {

                @Override
                public void onTextMessage(TextMessageEvent e) {
                        // Dont react to query's messages
                        if (e.getTargetMode() == TextMessageTargetMode.CHANNEL && e.getInvokerId() != clientId) {
                                String message = e.getMessage().toLowerCase();

                                if (!processText(message,e.getInvokerName(),api).equals("")){
                                api.sendChannelMessage(processText(message,e.getInvokerName(), api));
                                }
                        }
                }
        });
    }
}
