package pl.iot.raspberry;


import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.InputStreamReader;
import static java.lang.Thread.sleep;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.ResultSet;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;



public class CyclicSaver extends Thread {
    
	public String checkTemp(){
    
		float temperature = 0;
        String strLine = "";
		
        try{
            FileInputStream fstream = new FileInputStream("/sys/bus/w1/devices/28-0516934116ff/w1_slave");
            BufferedReader br = new BufferedReader(new InputStreamReader(fstream));
			int i = 0;
			
			while ((strLine = br.readLine()) != null)   {

				if(i == 1){
				String[] parts = strLine.split("=");

				temperature = Float.parseFloat(parts[1]);
				temperature = temperature/1000;
        
				}
			i++;
			}
			
			br.close();
        }catch(Exception e){}
		
        return temperature+"";
    }
	
	
     public void run(){
		 
        Statement stmt = null;
        ResultSet rs = null;
        Connection conn = null;
		
        String temp = "0";
		
		try {
            conn = DriverManager.getConnection("jdbc:mysql://mrokas.ayz.pl/mrokas_iot?user=mrokas_iot&password=testowe12");
            stmt = conn.createStatement();

            while(true) {

                DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                Date date = new Date();
                String dataa = dateFormat.format(date);

                temp = checkTemp();

                stmt.executeUpdate("INSERT INTO temperature (value, date) VALUES ('" + temp + "', '" + dataa + "');");
 
                System.out.println("Info sent: " + temp);
                sleep(5000);
            }
 
        }
        catch (SQLException ex){
            System.out.println("SQLException: " + ex.getMessage());
            System.out.println("SQLState: " + ex.getSQLState());
            System.out.println("VendorError: " + ex.getErrorCode());
        } catch (InterruptedException ex) {
            System.out.println("Sleep Error   "+ex);
        }
        finally {
 
            if (rs != null) {
                try {
                    rs.close();
                } catch (SQLException sqlEx) { }
 
                rs = null;
            }
 
            if (stmt != null) {
                try {
                    stmt.close();
                } catch (SQLException sqlEx) { }
 
                stmt = null;
            }
        }
		
    }
}
